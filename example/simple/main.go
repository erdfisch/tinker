package main

import (
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.com/erdfisch/tinker"
)

func main() {
	e := echo.New()
	e.Renderer = tinker.New()
	e.GET("/", func(c echo.Context) error {
		return c.Render(200, "default:index", echo.Map{"Name": "Tinker"})
	})

	log.Fatal(e.Start(":8080"))
}
