# Tinker

Simple [echo](https://echo.labstack.com) Template Renderer.

Example usage:
  - [simple](https://gitlab.com/erdfisch/tinker/tree/master/example/simple)