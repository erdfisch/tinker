package tinker

import (
	"bytes"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
)

type testContext struct {
	echo.Context
}

func TestLoad(t *testing.T) {
	tinker := New()
	tinker.Root = "example/simple/public/views"
	err := tinker.Load()
	if err != nil {
		t.Fatal(err)
	}

	var out bytes.Buffer
	err = tinker.Render(&out, "default:index", echo.Map{"Name": "Tinker"}, testContext{})
	if err != nil {
		t.Fatal(err)
	}

	expect := "Hello, Tinker!"
	got := out.String()
	if !strings.Contains(got, expect) {
		t.Errorf("expected \"%s\" in %s", expect, got)
	}
}

func TestDefaultLayout(t *testing.T) {
	tinker := New()
	tinker.Root = "example/simple/public/views"
	err := tinker.Load()
	if err != nil {
		t.Fatal(err)
	}

	var out bytes.Buffer
	err = tinker.Render(&out, "index", echo.Map{"Name": "Tinker"}, testContext{})
	if err != nil {
		t.Fatal(err)
	}

	expect := "Hello, Tinker!"
	got := out.String()
	if !strings.Contains(got, expect) {
		t.Errorf("expected \"%s\" in %s", expect, got)
	}
}
