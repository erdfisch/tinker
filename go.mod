module gitlab.com/erdfisch/tinker

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/labstack/echo/v4 v4.1.17
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620 // indirect
	golang.org/x/net v0.0.0-20201216054612-986b41b23924 // indirect
	golang.org/x/sys v0.0.0-20201218084310-7d0127a74742 // indirect
	golang.org/x/text v0.3.4 // indirect
)
