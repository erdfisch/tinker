package tinker

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/oxtoacart/bpool"
)

// Tinker struct.
type Tinker struct {
	// Template files extension.
	Ext string

	// Root folder for the templates.
	Root string

	// Path to layout templates, relative to Tinker.Root.
	Layouts string

	// Path to  page templates, relative to Tinker.Root.
	Pages string

	// Path to include templates, relative to Tinker.Root.
	Includes string

	// Default layout.
	DefaultLayout string

	// Optional FuncMap.
	FuncMap template.FuncMap

	// Reload templates on every render.
	Reload bool

	templates map[string]*template.Template
	bufpool   *bpool.BufferPool
	loaded    bool
}

// New  creates an instance of Tinker.
func New() *Tinker {
	t := &Tinker{
		Ext:       "html",
		Root:      "public/views",
		Layouts:   "layouts",
		Pages:     "pages",
		Includes:  "includes",
		DefaultLayout: "default",
		FuncMap:   make(template.FuncMap),
		templates: make(map[string]*template.Template),
		bufpool:   bpool.NewBufferPool(64),
	}
	return t
}

// Render implements the echo.Renderer interface.
func (t *Tinker) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.render(w, name, data)
}

// Load loads and parses all templates.
func (t *Tinker) Load() error {
	if t.templates == nil {
		t.templates = make(map[string]*template.Template)
	}

	layoutsPath := filepath.Join(t.Root, t.Layouts)
	layouts, err := getFiles(layoutsPath, t.Ext)
	if err != nil {
		return err
	}

	pagesPath := filepath.Join(t.Root, t.Pages)
	pages, err := getFiles(pagesPath, t.Ext)
	if err != nil {
		return err
	}

	includes, err := getFiles(filepath.Join(t.Root, t.Includes), t.Ext)
	if err != nil {
		return err
	}

	for _, layout := range layouts {
		for _, page := range pages {
			files := append(includes, layout, page)
			layoutName := getName(layoutsPath, layout)
			pageName := getName(pagesPath, page)

			tmpl := template.New(pageName)

			if len(t.FuncMap) > 0 {
				tmpl.Funcs(t.FuncMap)
			}

			for _, f := range files {
				content, err := ioutil.ReadFile(f)
				if err != nil {
					return err
				}
				template.Must(tmpl.Parse(string(content)))
			}

			t.templates[layoutName+":"+pageName] = tmpl
		}
	}

	return nil
}

func getFiles(root, ext string) ([]string, error) {
	var files []string
	ext = "."+ext
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ext {
			return nil
		}

		files = append(files, path)

		return nil
	})
	return files, err
}

func (t *Tinker) render(w io.Writer, name string, data interface{}) error {
	if !t.loaded || t.Reload {
		if err := t.Load(); err != nil {
			return fmt.Errorf("could not load template %s: %v", name, err)
		}
		t.loaded = true
	}

	if !strings.Contains(name, ":") {
		name = t.DefaultLayout + ":" + name
	}

	tmpl, ok := t.templates[name]
	if !ok {
		return fmt.Errorf("template %s does not exist", name)
	}

	buf := t.bufpool.Get()
	defer t.bufpool.Put(buf)

	if err := tmpl.ExecuteTemplate(buf, "layout", data); err != nil {
		return fmt.Errorf("could not execute template %s: %v", name, err)
	}

	if _, err := buf.WriteTo(w); err != nil {
		return fmt.Errorf("could not write template %s: %v", name, err)
	}

	return nil
}

func getName(root, filename string) string {
	name := strings.TrimPrefix(filename, root)
	name = strings.TrimPrefix(name, "/")
	return name[:strings.LastIndex(name, ".")]
}
